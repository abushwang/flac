%bcond_without hwcaps

Summary: An encoder/decoder for the Free Lossless Audio Codec
Name: flac
Version: 1.4.3
Release: 3%{?dist}
License: BSD and GPLv2+ and GFDL
URL: https://www.xiph.org/flac/
Source0: https://downloads.xiph.org/releases/flac/flac-%{version}.tar.xz

BuildRequires: libogg-devel gettext-devel doxygen
BuildRequires: gcc gcc-c++ make automake autoconf libtool
Requires: %{name}-libs = %{version}-%{release}

%description
FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3,
but lossless, meaning that audio is compressed in FLAC without any loss in
quality. This is similar to how Zip works, except with FLAC you will get
much better compression because it is designed specifically for audio, and
you can play back compressed FLAC files in your favorite player.

%package libs
Summary: Libraries for the Free Lossless Audio Codec

%description libs
This package provides the FLAC libraries.

%package devel
Summary: Development libraries and header files from FLAC
Requires: %{name}-libs = %{version}-%{release}
Requires: pkgconfig

%description devel
This package provides development files for %{name}.

%prep
%autosetup

%build
./autogen.sh -V

export CFLAGS="%{optflags} -funroll-loops"

mkdir normal
pushd normal
ln -s ../configure configure
%configure \
    --htmldir=%{_docdir}/flac/html \
    --disable-xmms-plugin \
    --disable-silent-rules \
    --disable-thorough-tests

%make_build
popd

%if %{with hwcaps}
mkdir hwcaps
pushd hwcaps
%ifarch x86_64
export CFLAGS=$(echo %optflags | sed 's/=x86-64-v2/=x86-64-v4/')
export CXXFLAGS=$(echo %optflags | sed 's/=x86-64-v2/=x86-64-v4/')
%endif
ln -s ../configure configure
%configure \
    --prefix=/drop%{_prefix} \
    --includedir=/drop/%{_includedir} \
    --libdir=%{_libdir}/glibc-hwcaps/x86-64-v4 \
    --htmldir=%{_docdir}/flac/html \
    --disable-xmms-plugin \
    --disable-silent-rules \
    --disable-thorough-tests

%make_build

popd
%endif

%install
pushd normal
%make_install
popd

%if %{with hwcaps}
pushd hwcaps
%make_install
popd

rm -rf %{buildroot}/drop
%endif

rm -r %{buildroot}%{_docdir}/flac
rm %{buildroot}%{_libdir}/*.la

%check
pushd normal
make check
popd

%files
%{_bindir}/flac
%{_bindir}/metaflac
%{_mandir}/man1/*

%files libs
%doc AUTHORS README.md CHANGELOG.md
%license COPYING.*
%{_libdir}/libFLAC.so.12*
%{_libdir}/libFLAC++.so.10*
%if %{with hwcaps}
%{_libdir}/glibc-hwcaps/x86-64-v4
%endif

%files devel
%doc doc/api
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*.m4

%changelog
* Tue Apr 2 2024 Shuo Wang <abushwang@tencent.com> - 1.4.3-3
- add support for glibc-hwcaps

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.3-2
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Aug 17 2023 kianli <kianli@tencent.com> - 1.4.3-1
- Upgrade to 1.4.3

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.3.4-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.3.4-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Nov 24 2022 cunshunxia <cunshunxia@tencent.com> - 1.3.4-1
- initial build
